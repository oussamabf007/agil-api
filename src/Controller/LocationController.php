<?php

namespace App\Controller;

use App\Entity\Location;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class LocationController extends AbstractController
{

    /**
     * @Route("/api/locations", name="list_locations", methods={"GET"})
     */
    public function index(): Response
    {
        $locations = $this->getDoctrine()->getRepository(Location::class)->findAllArray();

        return $this->json([
            "code" => 200,
            "data" => $locations
        ]);
    }

    /**
     * @Route("/api/locations/{id}", name="show_location", methods={"GET"})
     */
    public function show($id): Response
    {

        $em = $this->getDoctrine()->getManager();
        $location = $em->getRepository(Location::class)->findOneArray($id);

        return $this->json([
            "code" => 200,
            "data" => $location[0]
        ]);
    }

    /**
     * @Route("/api/locations", name="create_location", methods={"POST"})
     */
    public function createCategory(Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        if (!$name) {
            return $this->json([
                "code" => "400",
                "msg" => "verify your credentials"
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        $location = new Location();
        $location->setName($name);
        $location->setCreatedAt(new DateTime());


        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($location);
        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        return $this->json([
            "code" => 200,
            "msg" => "Location Created Successfully",
            "name" => $location->getName(),

        ]);
    }

    /**
     * @Route("/api/locations/{id}", name="delete_location", methods={"DELETE"})
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();

        $location = $em->getRepository(Location::class)->find($id);

        if (!$location) {
            return $this->json([
                "msg" => "Location does not exist",
            ]);
        }
        $em->remove($location);
        $em->flush();
        return $this->json([
            "msg" => "Location deleted"
        ]);
    }

    /**
     * @Route("/api/locations/{id}", name="edit_location", methods={"PUT"})
     */

    public function edit($id, Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        if (!$name) {
            return $this->json([
                "msg" => "name required",
            ]);
        }
        $em = $this->getDoctrine()->getManager();
        $location = $em->getRepository(Location::class)->find($id);
        if (!$location) {
            return $this->json([
                "msg" => "Location does not exist",
            ]);
        }
        $location->setName($name);
        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Location updated"
        ]);
    }
}
