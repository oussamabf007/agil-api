<?php

namespace App\Controller;

use App\Entity\Responsible;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ResponsibleController extends AbstractController
{

    /**
     * @Route("/api/responsibles", name="list_responsables", methods={"GET"})
     */
    public function index(): Response
    {
        $responsibles = $this->getDoctrine()->getRepository(Responsible::class)->findAllArray();

        return $this->json([
            "code" => 200,
            "data" => $responsibles
        ]);
    }

    /**
     * @Route("/api/responsibles/{id}", name="show_responsible", methods={"GET"})
     */
    public function show($id): Response
    {

        $em = $this->getDoctrine()->getManager();
        $responsible = $em->getRepository(Responsible::class)->findOneArray($id);

        return $this->json([
            "code" => 200,
            "data" => $responsible[0]
        ]);
    }

    /**
     * @Route("/api/responsibles", name="create_responsable", methods={"POST"})
     */
    public function createResponsible(Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        if (!$name) {
            return $this->json([
                "code" => "400",
                "msg" => "verify your credentials"
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        $responsible = new Responsible();
        $responsible->setName($name);
        $responsible->setCreatedAt(new DateTime());


        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($responsible);
        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        return $this->json([
            'code' => 200,
            "msg" => "Responsible Created Successfully",
            "name" => $responsible->getName(),

        ]);
    }
    /**
     * @Route("/api/responsibles/{id}", name="delete_responsable", methods={"DELETE"})
     */
    public function delete($id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $responsible = $em->getRepository(Responsible::class)->find($id);
        if (!$responsible) {
            return $this->json([
                "msg" => "Responsible does not exist",
            ]);
        }
        $em->remove($responsible);
        $em->flush();

        return $this->json([
            "msg" => "Responsible deleted",
        ]);
    }

    /**
     * @Route("/api/responsibles/{id}", name="edit_responsible", methods={"PUT"})
     */

    public function edit($id, Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        if (!$name) {
            return $this->json([
                "msg" => "name required",
            ]);
        }
        $em = $this->getDoctrine()->getManager();
        $responsible = $em->getRepository(Responsible::class)->find($id);
        if (!$responsible) {
            return $this->json([
                "msg" => "Responsible does not exist",
            ]);
        }
        $responsible->setName($name);
        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Responsible updated"
        ]);
    }
}
