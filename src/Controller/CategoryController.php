<?php

namespace App\Controller;

use App\Entity\Category;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends AbstractController
{

    /**
     * @Route("/api/categories", name="list_categories", methods={"GET"})
     */
    public function index(): Response
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAllArray();

        return $this->json([
            "code" => 200,
            "data" => $categories
        ]);
    }

    /**
     * @Route("/api/categories/{id}", name="show_category", methods={"GET"})
     */
    public function show($id): Response
    {

        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository(Category::class)->findOneArray($id);

        return $this->json([
            "code" => 200,
            "data" => $category[0]
        ]);
    }

    /**
     * @Route("/api/categories", name="create_category", methods={"POST"})
     */
    public function createCategory(Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        if (!$name) {
            return $this->json([
                "code" => "400",
                "msg" => "verify your credentials"
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        $category = new Category();
        $category->setName($name);
        $category->setCreatedAt(new DateTime());


        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($category);
        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        return $this->json([
            "code" => 200,
            "msg" => "Category Created Successfully",
            "name" => $category->getName(),

        ]);
    }

    /**
     * @Route("/api/categories/{id}", name="delete_category", methods={"DELETE"})
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository(Category::class)->find($id);

        if (!$category) {
            return $this->json([
                "msg" => "Category does not exist",
            ]);
        }
        $em->remove($category);
        $em->flush();
        return $this->json([
            "msg" => "Category deleted"
        ]);
    }

    /**
     * @Route("/api/categories/{id}", name="edit_category", methods={"PUT"})
     */

    public function edit($id, Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        if (!$name) {
            return $this->json([
                "msg" => "name required",
            ]);
        }
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository(Category::class)->find($id);
        if (!$category) {
            return $this->json([
                "msg" => "Category does not exist",
            ]);
        }
        $category->setName($name);
        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Category updated"
        ]);
    }
}
