<?php

namespace App\Controller;

use App\Entity\Emplacement;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class EmplacementController extends AbstractController
{
    /**
     * @Route("/api/emplacements", name="list_emplacements", methods={"GET"})
     */
    public function index(): Response
    {
        $emplacements = $this->getDoctrine()->getRepository(Emplacement::class)->findAllArray();

        return $this->json([
            "code" => 200,
            "data" => $emplacements
        ]);
    }

    /**
     * @Route("/api/emplacements/{id}", name="show_emplacement", methods={"GET"})
     */
    public function show($id): Response
    {

        $em = $this->getDoctrine()->getManager();
        $emplacement = $em->getRepository(Emplacement::class)->findOneArray($id);

        return $this->json([
            "code" => 200,
            "data" => $emplacement[0]
        ]);
    }

    /**
     * @Route("/api/emplacements", name="create_emplacement", methods={"POST"})
     */
    public function createEmplacement(Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        if (!$name) {
            return $this->json([

                "code" => 400,
                "msg" => "verify your credentials"
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        $emplacement = new Emplacement();
        $emplacement->setName($name);
        $emplacement->setCreatedAt(new DateTime());


        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($emplacement);
        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        return $this->json([
            "code" => 200,
            "msg" => "Emplacement Created Successfully",
            "name" => $emplacement->getName(),

        ]);
    }

    /**
     * @Route("/api/emplacements/{id}", name="delete_emplacement", methods={"DELETE"})
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();
        $emplacement = $em->getRepository(Emplacement::class)->find($id);
        if (!$emplacement) {
            return $this->json([
                "msg" => "Emplacement does not exist",
            ]);
        }
        $em->remove($emplacement);
        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Emplacement deleted"
        ]);
    }

    /**
     * @Route("/api/emplacements/{id}", name="edit_emplacement", methods={"PUT"})
     */

    public function edit($id, Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        if (!$name) {
            return $this->json([
                "msg" => "name required",
            ]);
        }
        $em = $this->getDoctrine()->getManager();
        $emplacement = $em->getRepository(Emplacement::class)->find($id);
        if (!$emplacement) {
            return $this->json([
                "msg" => "Emplacement does not exist",
            ]);
        }
        $emplacement->setName($name);
        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Emplacement updated"
        ]);
    }
}
