<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Concerne;
use App\Entity\Controle;
use App\Entity\Emplacement;
use App\Entity\Evenement;
use App\Entity\Frequence;
use App\Entity\Imprime;
use App\Entity\Responsible;
use App\Entity\Location;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


class EvenementController extends AbstractController
{
    /**
     * @Route("/api/evenements/init-event", name="init_evenement", methods={"GET"})
     */
    public function initEvent()
    {

        $frequences = $this->getDoctrine()->getRepository(Frequence::class)->findAllArray();
        $imprimes = $this->getDoctrine()->getRepository(Imprime::class)->findAllArray();
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAllArray();
        $concernes = $this->getDoctrine()->getRepository(Concerne::class)->findAllArray();
        $controles = $this->getDoctrine()->getRepository(Controle::class)->findAllArray();
        $responsibles = $this->getDoctrine()->getRepository(Responsible::class)->findAllArray();
        $locations = $this->getDoctrine()->getRepository(Location::class)->findAllArray();
        $emplacements = $this->getDoctrine()->getRepository(Emplacement::class)->findAllArray();


        return $this->json([
            "code" => 200,
            "msg" => "init event succeded",
            "data" => [
                "frequences" => $frequences,
                "imprimes" => $imprimes,
                "categories" => $categories,
                "concernes" => $concernes,
                "controles" => $controles,
                "responsibles" => $responsibles,
                "locations" => $locations,
                "emplacements" => $emplacements,

            ]
        ]);
    }

    /**
     * @Route("/api/evenements", name="list_evenements", methods={"GET"})
     */
    public function index(): Response
    {
        $evenements = $this->getDoctrine()->getRepository(Evenement::class)->findAllArray();

        return $this->json([
            "code" => 200,
            "data" => $evenements
        ]);
    }

    /**
     * @Route("/api/evenements/user", name="list_event_user", methods={"GET"})
     */
    public function getByUser(): Response
    {
        $connected_user = $this->getUser();
        $locationId = $connected_user->getLocation()->getId();

        $evenements = $this->getDoctrine()->getRepository(Evenement::class)->findByUser($locationId);

        return $this->json([
            "code" => 200,
            "data" => $evenements
        ]);
    }




    /**
     * @Route("/api/evenements/{id}", name="show_evenement", methods={"GET"})
     */
    public function show($id): Response
    {

        $em = $this->getDoctrine()->getManager();
        $evenement = $em->getRepository(Evenement::class)->findOneArray($id);

        return $this->json([
            "code" => 200,
            "data" => $evenement[0]
        ]);
    }

    /**
     * @Route("/api/evenements", name="create_evenement", methods={"POST"})
     */
    public function createEvenement(Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        $jour = $req->jour ?? null;
        $semaine = $req->semaine ?? null;
        $date = $req->date ?? null;
        $freqId = $req->frequence;
        $impId = $req->imprime;
        $catId = $req->category;
        $concId = $req->concerne;
        $contId = $req->controle;
        $resId = $req->responsible;
        $locId = $req->location;
        $empId = $req->emplacement;
        if (!$name) {
            return $this->json([
                "code" => "400",
                "msg" => "verify your credentials"
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        $frequence = $em->getRepository(Frequence::class)->find($freqId);
        $imprime = $em->getRepository(Imprime::class)->find($impId);
        $category = $em->getRepository(Category::class)->find($catId);
        $concerne = $em->getRepository(Concerne::class)->find($concId);
        $controle = $em->getRepository(Controle::class)->find($contId);
        $responsible = $em->getRepository(Responsible::class)->find($resId);
        $location = $em->getRepository(Location::class)->find($locId);
        $emplacement = $em->getRepository(Emplacement::class)->find($empId);

        // $evenement = new Evenement();
        $evenement = $this->getDoctrine()->getRepository(Evenement::class)->findByName($name);

        if ($evenement) {
            return $this->json([
                "code" => 400,
                "msg" => "Evenement existe avec le même nom"
            ]);
        }


        $evenement = new Evenement();
        $evenement->setName($name);
        if ($jour)
            $evenement->setJour($jour);
        if ($semaine)
            $evenement->setSemaine($semaine);
        if ($date)
            $evenement->setDate(new DateTime(date('Y-m-d', strtotime($date))));
        $evenement->setCreatedAt(new DateTime());
        $evenement->setFrequence($frequence);
        $evenement->setImprime($imprime);
        $evenement->setCategory($category);
        $evenement->setConcerne($concerne);
        $evenement->setControle($controle);
        $evenement->setResponsible($responsible);
        $evenement->setLocation($location);
        $evenement->setEmplacement($emplacement);
        $evenement->setEtat(true);


        // dd($evenement);
        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($evenement);
        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        return $this->json([
            'code' => 200,
            "msg" => "Evenement Created Successfully",
            "name" => $evenement->getName(),

        ]);
    }

    /**
     * @Route("/api/evenements/{id}", name="delete_evenements", methods={"DELETE"})
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();
        $evenement = $em->getRepository(Evenement::class)->find($id);
        if (!$evenement) {
            return $this->json([
                "msg" => "Evenement does not exist",
            ]);
        }
        $em->remove($evenement);
        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Evenement deleted"
        ]);
    }


    /**
     * @Route("/api/evenements/{id}", name="edit_evenement", methods={"PUT"})
     */
    public function edit($id, Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        $jour = $req->jour ?? null;
        $semaine = $req->semaine ?? null;
        $date = $req->date ?? null;
        $freqId = $req->frequence;
        $impId = $req->imprime;
        $catId = $req->category;
        $concId = $req->concerne;
        $contId = $req->controle;
        $resId = $req->responsible;
        $locId = $req->location;
        $empId = $req->emplacement;
        if (!$name) {
            return $this->json([
                "code" => "400",
                "msg" => "verify your credentials"
            ]);
        }
        if (!$name) {
            return $this->json([
                "msg" => "name required",
            ]);
        }
        $em = $this->getDoctrine()->getManager();
        $evenement = $em->getRepository(Evenement::class)->find($id);
        if (!$evenement) {
            return $this->json([
                "msg" => "Evenement does not exist",
            ]);
        }
        $frequence = $em->getRepository(Frequence::class)->find($freqId);
        $imprime = $em->getRepository(Imprime::class)->find($impId);
        $category = $em->getRepository(Category::class)->find($catId);
        $concerne = $em->getRepository(Concerne::class)->find($concId);
        $controle = $em->getRepository(Controle::class)->find($contId);
        $responsible = $em->getRepository(Responsible::class)->find($resId);
        $location = $em->getRepository(Location::class)->find($locId);
        $emplacement = $em->getRepository(Emplacement::class)->find($empId);

        $evenement->setName($name);
        // if ($jour)
        $evenement->setJour($jour);
        // if ($semaine)
        $evenement->setSemaine($semaine);
        if ($date) {
            $evenement->setDate(new DateTime(date('Y-m-d', strtotime($date))));
        } else {
            $evenement->setDate(null);
        }





        $evenement->setFrequence($frequence);
        $evenement->setImprime($imprime);
        $evenement->setCategory($category);
        $evenement->setConcerne($concerne);
        $evenement->setControle($controle);
        $evenement->setResponsible($responsible);
        $evenement->setLocation($location);
        $evenement->setEmplacement($emplacement);

        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Evenement updated",
            "name" => $evenement->getName(),
        ]);
    }



    /**
     * @Route("/api/evenements/on_hold/{id}", name="pause_evenement", methods={"PUT"})
     */
    public function hold($id, Request $request)
    {
        $req = json_decode($request->getContent());

        $etat = $req->etat;
        $em = $this->getDoctrine()->getManager();
        $evenement = $em->getRepository(Evenement::class)->find($id);

        if ($etat === true) {
            $evenement->setEtat(false);
        } else {
            $evenement->setEtat(true);
        }

        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Evenement updated",
            "name" => $evenement->getName(),
        ]);
    }
}
