<?php

namespace App\Controller;

use App\Entity\Frequence;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class FrequenceController extends AbstractController
{

    /**
     * @Route("/api/frequences", name="list_frequences", methods={"GET"})
     */
    public function index(): Response
    {
        $frequences = $this->getDoctrine()->getRepository(Frequence::class)->findAllArray();

        return $this->json([
            "code" => 200,
            "data" => $frequences
        ]);
    }

    /**
     * @Route("/api/frequences/{id}", name="show_frequence", methods={"GET"})
     */
    public function show($id): Response
    {

        $em = $this->getDoctrine()->getManager();
        $frequence = $em->getRepository(Frequence::class)->findOneArray($id);

        return $this->json([
            "code" => 200,
            "data" => $frequence[0]
        ]);
    }

    /**
     * @Route("/api/frequences", name="create_frequence", methods={"POST"})
     */
    public function create(Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        if (!$name) {
            return $this->json([
                "code" => 400,
                "msg" => "verify your credentials"
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        $frequence = new Frequence();
        $frequence->setName($name);
        $frequence->setCreatedAt(new DateTime());


        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($frequence);
        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        return $this->json([
            "code" => 200,
            "msg" => "Frequence Created Successfully",
            "name" => $frequence->getName(),

        ]);
    }

    /**
     * @Route("/api/frequences/{id}", name="delete_frequence", methods={"DELETE"})
     */

    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();
        $frequence = $em->getRepository(Frequence::class)->find($id);
        if (!$frequence) {
            return $this->json([
                "msg" => "Frequence does not exist",
            ]);
        }
        $em->remove($frequence);
        $em->flush();
        return $this->json([
            "msg" => "Frequence deleted"
        ]);
    }

    /**
     * @Route("/api/frequences/{id}", name="edit_frequence", methods={"PUT"})
     */

    public function edit($id, Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        if (!$name) {
            return $this->json([
                "msg" => "name required",
                "code" => 400
            ]);
        }
        $em = $this->getDoctrine()->getManager();
        $frequence = $em->getRepository(Frequence::class)->find($id);
        if (!$frequence) {
            return $this->json([
                "code" => 404,
                "msg" => "Frequence does not exist",
            ]);
        }
        $frequence->setName($name);
        $em->flush();
        return $this->json([
            "msg" => "Frequence updated",
            "code" => 200
        ]);
    }
}
