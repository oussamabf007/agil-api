<?php

namespace App\Controller;

use App\Entity\Controle;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ControleController extends AbstractController
{

    /**
     * @Route("/api/controles", name="list_controles", methods={"GET"})
     */
    public function index(): Response
    {
        $controles = $this->getDoctrine()->getRepository(Controle::class)->findAllArray();

        return $this->json([
            "code" => 200,
            "data" => $controles
        ]);
    }

    /**
     * @Route("/api/controles/{id}", name="show_controle", methods={"GET"})
     */
    public function show($id): Response
    {

        $em = $this->getDoctrine()->getManager();
        $controle = $em->getRepository(Controle::class)->findOneArray($id);

        return $this->json([
            "code" => 200,
            "data" => $controle[0]
        ]);
    }

    /**
     * @Route("/api/controles", name="create_controle", methods={"POST"})
     */
    public function createControle(Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        if (!$name) {
            return $this->json([

                "code" => 400,
                "msg" => "verify your credentials"
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        $controle = new Controle();
        $controle->setName($name);
        $controle->setCreatedAt(new DateTime());


        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($controle);
        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        return $this->json([
            "code" => 200,
            "msg" => "Controle Created Successfully",
            "name" => $controle->getName(),

        ]);
    }

    /**
     * @Route("/api/controles/{id}", name="delete_controle", methods={"DELETE"})
     */

    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();
        $controle = $em->getRepository(Controle::class)->find($id);
        if (!$controle) {
            return $this->json([
                "msg" => "Controle does not exist",
            ]);
        }
        $em->remove($controle);
        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Controle deleted"
        ]);
    }

    /**
     * @Route("/api/controles/{id}", name="edit_controle", methods={"PUT"})
     */

    public function edit($id, Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        if (!$name) {
            return $this->json([
                "msg" => "name required",
            ]);
        }
        $em = $this->getDoctrine()->getManager();
        $controle = $em->getRepository(Controle::class)->find($id);
        if (!$controle) {
            return $this->json([
                "msg" => "Controle does not exist",
            ]);
        }
        $controle->setName($name);
        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Controle updated"
        ]);
    }
}
