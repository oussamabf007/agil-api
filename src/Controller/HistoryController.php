<?php

namespace App\Controller;

use App\Entity\Evenement;
use App\Entity\History;
use App\Entity\HistoryInit;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HistoryController extends AbstractController
{
    /**
     * @Route("/api/history", name="list_history", methods={"GET"})
     */
    public function index(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $connected_user = $this->getUser();
        $locationId = $connected_user->getLocation()->getId();

        // verify if history initialized
        $init = $em->getRepository(HistoryInit::class)->findOneBy(["date" => new DateTime()]);

        if (!$init) {
            // search for todays events
            // $events = $em->getRepository(Evenement::class)->findByDate();
            $events = $this->getTodaysEvent();

            //save events in history table
            foreach ($events as $event) {
                $history = new History();
                $history->setEvent($event);
                $history->setCreatedAt(new DateTime());
                $history->setStatus(false);
                $em->persist($history);
            }

            $initH = new HistoryInit();
            $initH->setDate(new DateTime());
            $initH->setExecuted(true);
            $em->persist($initH);

            $em->flush();
        }
        // return all events from history
        $todaysHistory = $em->getRepository(History::class)->findByDateAndStatus(false, date("Y-m-d"), $locationId);

        return $this->json([
            "code" => 200,
            "data" => $todaysHistory
        ]);
    }


    /**
     * @Route("/tasks", name="tasks_lists", methods={"GET"})
     */
    public function front(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $connected_user = $this->getUser();
        $locationId = $connected_user->getLocation()->getId();

        // verify if history initialized
        $init = $em->getRepository(HistoryInit::class)->findOneBy(["date" => new DateTime(), "executed" => true]);

        if (!$init) {
            $initH = new HistoryInit();
            $initH->setDate(new DateTime());
            $initH->setExecuted(true);
            $em->persist($initH);
            $em->flush();

            // search for todays events
            // $events = $em->getRepository(Evenement::class)->findByDate();
            try {

                $events = $this->getTodaysEvent();

                //save events in history table
                foreach ($events as $event) {
                    $history = new History();
                    $history->setEvent($event);
                    $history->setCreatedAt(new DateTime());
                    $history->setStatus(false);
                    $em->persist($history);
                }

                $em->flush();
            } catch (\Throwable $th) {
                //throw $th;
                $initH->setExecuted(false);
                $em->flush();
            }
        }

        // return all events from history
        $todaysHistory = $em->getRepository(History::class)->findByDateAndStatus(false, date("Y-m-d"), $locationId);


        $tasksByCat = array();
        foreach ($todaysHistory as $task) {

            $categoryName =  $task["event"]["category"]["name"];


            $tasksByCat[$categoryName][] = $task;
        }

        return $this->json([
            "code" => 200,
            "data" => $tasksByCat
        ]);
    }

    /**
     * @Route("/api/history/{id}", name="delete_history", methods={"DELETE"})
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();

        $history = $em->getRepository(History::class)->find($id);
        if (!$history) {
            return $this->json([
                "code" => 400,
                "msg" => "Evenement does not exist",
            ]);
        }
        $em->remove($history);
        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Evenement supprimé avec succès"
        ]);
    }

    /**
     * @Route("/api/history/validate-all", name="validate_all_history", methods={"POST"})
     */
    public function validateALl()
    {

        $em = $this->getDoctrine()->getManager();

        $connected_user = $this->getUser();

        $locationId = $connected_user->getLocation()->getId();

        $todaysHistory = $em->getRepository(History::class)->findByDateAndStatus(false, date("Y-m-d"), $locationId);




        foreach ($todaysHistory as $hist) {
            $history = $em->getRepository(History::class)->find($hist["id"]);

            $event = $history->getEvent();
            $eventName = $event->getFrequence()->getName();
            $nextEventDate = "";
            // if frequence == trimestriel ==> +3mois
            switch ($eventName) {
                case 'trimestriel':
                    $eventDate = $event->getDate();
                    if ($eventDate->format("m") === date("m")) {
                        $nextEventDate = date("Y-m-d H:i:s", strtotime($eventDate->format('Y-m-d') . " +3 months"));

                        $event->setDate(new DateTime($nextEventDate));
                    }
                    break;

                case 'semestriel':
                    $eventDate = $event->getDate();
                    if ($eventDate->format("m") === date("m")) {
                        $nextEventDate = date("Y-m-d H:i:s", strtotime($eventDate->format('Y-m-d') . " +6 months"));

                        $event->setDate(new DateTime($nextEventDate));
                    }
                    break;

                case 'annuel':
                    $eventDate = $event->getDate();
                    if ($eventDate->format("m") === date("m")) {
                        $nextEventDate = date("Y-m-d H:i:s", strtotime($eventDate->format('Y-m-d') . " +1 year"));

                        $event->setDate(new DateTime($nextEventDate));
                    }
                    break;

                case '02 ANS':
                    $eventDate = $event->getDate();
                    if ($eventDate->format("m") === date("m")) {
                        $nextEventDate = date("Y-m-d H:i:s", strtotime($eventDate->format('Y-m-d') . " +2 years"));

                        $event->setDate(new DateTime($nextEventDate));
                    }
                    break;

                case '03 ANS':
                    $eventDate = $event->getDate();
                    if ($eventDate->format("m") === date("m")) {
                        $nextEventDate = date("Y-m-d H:i:s", strtotime($eventDate->format('Y-m-d') . " +3 years"));

                        $event->setDate(new DateTime($nextEventDate));
                    }
                    break;

                case '05 ANS':
                    $eventDate = $event->getDate();
                    if ($eventDate->format("m") === date("m")) {
                        $nextEventDate = date("Y-m-d H:i:s", strtotime($eventDate->format('Y-m-d') . " +5 years"));

                        $event->setDate(new DateTime($nextEventDate));
                    }
                    break;

                case '10 ANS':
                    $eventDate = $event->getDate();
                    if ($eventDate->format("m") === date("m")) {
                        $nextEventDate = date("Y-m-d H:i:s", strtotime($eventDate->format('Y-m-d') . " +10 years"));

                        $event->setDate(new DateTime($nextEventDate));
                    }
                    break;

                default:
                    break;
            }

            $history->setValidatedAt(new DateTime());


            $history->setStatus(true);

            $em->flush();
        }

        return $this->json([
            "code" => 200,
            "msg" => "Tous les evenements sont validés"
        ]);
    }

    /**
     * @Route("/api/history/{id}", name="edit_history", methods={"PUT"})
     */
    public function edit($id)
    {
        //get entity manager
        $em = $this->getDoctrine()->getManager();

        // 1 jiiib el history with id
        $history = $em->getRepository(History::class)->find($id);

        $event = $history->getEvent();
        $eventName = $event->getFrequence()->getName();
        $nextEventDate = "";
        // if frequence == trimestriel ==> +3mois
        switch ($eventName) {
            case 'trimestriel':
                $eventDate = $event->getDate();
                if ($eventDate->format("m") === date("m")) {
                    $nextEventDate = date("Y-m-d H:i:s", strtotime($eventDate->format('Y-m-d') . " +3 months"));

                    $event->setDate(new DateTime($nextEventDate));
                }
                break;

            case 'semestriel':
                $eventDate = $event->getDate();
                if ($eventDate->format("m") === date("m")) {
                    $nextEventDate = date("Y-m-d H:i:s", strtotime($eventDate->format('Y-m-d') . " +6 months"));

                    $event->setDate(new DateTime($nextEventDate));
                }
                break;

            case 'annuel':
                $eventDate = $event->getDate();
                if ($eventDate->format("m") === date("m")) {
                    $nextEventDate = date("Y-m-d H:i:s", strtotime($eventDate->format('Y-m-d') . " +1 year"));

                    $event->setDate(new DateTime($nextEventDate));
                }
                break;

            case '02 ANS':
                $eventDate = $event->getDate();
                if ($eventDate->format("m") === date("m")) {
                    $nextEventDate = date("Y-m-d H:i:s", strtotime($eventDate->format('Y-m-d') . " +2 years"));

                    $event->setDate(new DateTime($nextEventDate));
                }
                break;

            case '03 ANS':
                $eventDate = $event->getDate();
                if ($eventDate->format("m") === date("m")) {
                    $nextEventDate = date("Y-m-d H:i:s", strtotime($eventDate->format('Y-m-d') . " +3 years"));

                    $event->setDate(new DateTime($nextEventDate));
                }
                break;

            case '05 ANS':
                $eventDate = $event->getDate();
                if ($eventDate->format("m") === date("m")) {
                    $nextEventDate = date("Y-m-d H:i:s", strtotime($eventDate->format('Y-m-d') . " +5 years"));

                    $event->setDate(new DateTime($nextEventDate));
                }
                break;

            case '10 ANS':
                $eventDate = $event->getDate();
                if ($eventDate->format("m") === date("m")) {
                    $nextEventDate = date("Y-m-d H:i:s", strtotime($eventDate->format('Y-m-d') . " +10 years"));

                    $event->setDate(new DateTime($nextEventDate));
                }
                break;

            default:
                break;
        }

        // MOCHKLA if Validated w na7a tzid + infini
        $status =  $history->getStatus();

        if ($status) {
            $history->setValidatedAt(null);
        } else {
            $history->setValidatedAt(new DateTime());
        }

        $history->setStatus(!$status);

        $em->flush();

        return $this->json([
            "code" => 200,
            "msg" => "updated successfully"
        ]);
    }

    public function getTodaysEvent()
    {
        $eventsToSave = array();
        $em = $this->getDoctrine()->getManager();
        // journaliers fesfes
        $journaliers = $em->getRepository(Evenement::class)->findJournaliers();

        // hebdomadaire get todays name
        $todaysDayNumber = date('w');
        $daysOfTheWeek = [
            0 => "dimanche",
            1 => "lundi",
            2 => "mardi",
            3 => "mercredi",
            4 => "jeudi",
            5 => "vendredi",
            6 => "samedi",
        ];
        $dayName = $daysOfTheWeek[$todaysDayNumber];
        $hebdomadaires = $em->getRepository(Evenement::class)->findHebdomadaire($dayName);

        // mensuel get todays name and week (week number in current month)
        $weekNumberInMonth = $this->weekOfMonth(strtotime(date('Y-m-d')));
        $mensuels = $em->getRepository(Evenement::class)->findMensuel($dayName, $weekNumberInMonth);

        // trimestriel & semestriel & annuel & occasionnel => by todays date
        $todayDate = date("Y-m-d");
        $triSeAnOccs = $em->getRepository(Evenement::class)->findTriSeAnOcc($todayDate);


        foreach ($journaliers as $journalier) {
            $eventsToSave[] = $journalier;
        }

        foreach ($hebdomadaires as $hebdomadaire) {
            $eventsToSave[] = $hebdomadaire;
        }

        foreach ($mensuels as $mensuel) {
            $eventsToSave[] = $mensuel;
        }


        foreach ($triSeAnOccs as $triSeAnOcc) {
            $eventsToSave[] = $triSeAnOcc;
        }

        // save trimestriel in history + 3months
        // save semestriel in history +6months
        // save annuel in history +1year
        // occasionel fesfes
        // get events with status "not validated"

        return $eventsToSave;
    }

    public function weekOfMonth($date)
    {
        //Get the first day of the month.
        $firstOfMonth = strtotime(date("Y-m-01", $date));
        //Apply above formula.
        return $this->weekOfYear($date) - $this->weekOfYear($firstOfMonth) + 1;
    }

    public function weekOfYear($date)
    {
        $weekOfYear = intval(date("W", $date));
        if (date('n', $date) == "1" && $weekOfYear > 51) {
            // It's the last week of the previos year.
            $weekOfYear = 0;
        }
        return $weekOfYear;
    }
}
