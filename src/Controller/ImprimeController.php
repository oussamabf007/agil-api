<?php

namespace App\Controller;

use App\Entity\Imprime;
use DateTime;
use Doctrine\ORM\Query\Expr\Andx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ImprimeController extends AbstractController
{

    /**
     * @Route("/api/imprimes", name="list_imprimes", methods={"GET"})
     */
    public function index(): Response
    {
        $imprimes = $this->getDoctrine()->getRepository(Imprime::class)->findAllArray();

        return $this->json([
            "code" => 200,
            "data" => $imprimes
        ]);
    }

    /**
     * @Route("/api/imprimes/{id}", name="show_imprime", methods={"GET"})
     */
    public function show($id): Response
    {

        $em = $this->getDoctrine()->getManager();
        $imprime = $em->getRepository(Imprime::class)->findOneArray($id);

        return $this->json([
            "code" => 200,
            "data" => $imprime[0]
        ]);
    }

    /**
     * @Route("/api/imprimes", name="create_imprime", methods={"POST"})
     */
    public function createControle(Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        $description = $req->description ?? "";
        if (!$name) {
            return $this->json([

                "code" => 400,
                "msg" => "verify your credentials"
            ]);
        }
        $em = $this->getDoctrine()->getManager();

        $imprime = new Imprime();
        $imprime->setName($name);
        $imprime->setDescription($description);
        $imprime->setCreatedAt(new DateTime());


        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($imprime);
        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        return $this->json([
            "code" => 200,
            "msg" => "Imprime Created Successfully",
            "name" => $imprime->getName(),

        ]);
    }

    /**
     * @Route("/api/imprimes/{id}", name="delete_imprime", methods={"DELETE"})
     */

    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();
        $imprime = $em->getRepository(Imprime::class)->find($id);
        if (!$imprime) {
            return $this->json([
                "msg" => "Imprime does not exist",
            ]);
        }
        $em->remove($imprime);
        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Imprime deleted"
        ]);
    }

    /**
     * @Route("/api/imprimes/{id}", name="edit_concerne", methods={"PUT"})
     */

    public function edit($id, Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        $description = $req->description ?? "";
        if (!$name) {
            return $this->json([
                "msg" => "name required",
            ]);
        }
        $em = $this->getDoctrine()->getManager();
        $imprime = $em->getRepository(Imprime::class)->find($id);
        if (!$imprime) {
            return $this->json([
                "msg" => "Imprime does not exist",
            ]);
        }
        $imprime->setName($name);
        if ($description)
            $imprime->setDescription($description);
        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Imprime updated"
        ]);
    }
}
