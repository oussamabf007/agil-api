<?php

namespace App\Controller;

use App\Entity\Concerne;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ConcerneController extends AbstractController
{

    /**
     * @Route("/api/concernes", name="list_concernes", methods={"GET"})
     */
    public function index(): Response
    {
        $concernes = $this->getDoctrine()->getRepository(Concerne::class)->findAllArray();

        return $this->json([
            "code" => 200,
            "data" => $concernes
        ]);
    }

    /**
     * @Route("/api/concernes/{id}", name="show_concerne", methods={"GET"})
     */
    public function show($id): Response
    {

        $em = $this->getDoctrine()->getManager();
        $concerne = $em->getRepository(Concerne::class)->findOneArray($id);

        return $this->json([
            "code" => 200,
            "data" => $concerne[0]
        ]);
    }

    /**
     * @Route("/api/concernes", name="create_concerne", methods={"POST"})
     */
    public function createControle(Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        if (!$name) {
            return $this->json([

                "code" => 400,
                "msg" => "verify your credentials"
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        $concerne = new Concerne();
        $concerne->setName($name);
        $concerne->setCreatedAt(new DateTime());


        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($concerne);
        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        return $this->json([
            "code" => 200,
            "msg" => "Concerne Created Successfully",
            "name" => $concerne->getName(),

        ]);
    }

    /**
     *@Route("/api/concernes/{id}", name="edit_concernes", methods={"PUT"})
     */
    public function editer($id, Request $request)
    {
        $req = json_decode($request->getContent());

        $name = $req->name;
        if (!$name) {
            return $this->json([
                "msg" => "name required",
            ]);
        }
        $em = $this->getDoctrine()->getManager();
        $concerne = $em->getRepository(Concerne::class)->find($id);
        if (!$concerne) {
            return $this->json([
                "msg" => "Concerne does not exist",
            ]);
        }
        $concerne->setName($name);
        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Concerne updated"
        ]);
    }

    /**
     * @Route("/api/concernes/{id}", name="delete_concerne", methods={"DELETE"})
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();
        $concerne = $em->getRepository(Concerne::class)->find($id);
        if (!$concerne) {
            return $this->json([
                "msg" => "Concerne does not exist",
            ]);
        }
        $em->remove($concerne);
        $em->flush();
        return $this->json([
            "code" => 200,
            "msg" => "Concerne deleted"
        ]);
    }
}
