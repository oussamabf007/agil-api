<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/register", name="new_user", methods={"POST"})
     */
    public function create(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $req = json_decode($request->getContent());

        $email = $req->email;
        $password = $req->password;

        if (!$email || !$password) {
            return $this->json([
                "code" => "400",
                "msg" => "verify your credentials"
            ]);
        }

        $em = $this->getDoctrine()->getManager();

        $user = new User();
        $user->setEmail($email);
        $user->setRoles(["ROLE_ADMIN"]);
        $user->setPassword($encoder->encodePassword($user, $password));

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($user);
        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        return $this->json([
            "msg" => "User Created Successfully",
            "email" => $user->getEmail(),
            "role" => $user->getRoles()
        ]);
    }


    /**
     * @Route("/api/update-password", name="update_password", methods={"POST"})
     */
    public function updateUserPass(Request $request, UserPasswordEncoderInterface $encoder, UserRepository $userRepository, EntityManagerInterface $em, MailerInterface $mailer)
    {
        $req = json_decode($request->getContent());

        $password = $req->password ?? "";

        if (!$password) {
            return $this->json(["code" => 400, "msg" => "Password required"], 400);
        }

        $user = $this->getUser();

        $user = $userRepository->find($user->getId());

        if (!$user) {
            return $this->json(["code" => 404, "msg" => "User not found"], 404);
        }

        $user->setPassword($encoder->encodePassword($user, $password));

        $em->persist($user);
        $em->flush();

        return $this->json(["code" => 200, "msg" => "Password updated successfully"], 200);
    }

    /**
     * @Route("/api/reset-password", name="reset_password", methods={"POST"})
     */
    public function resetPass(Request $request, UserPasswordEncoderInterface $encoder, UserRepository $userRepository, EntityManagerInterface $em, MailerInterface $mailer)
    {
        $req = json_decode($request->getContent());

        $email = $req->email ?? "";

        $user = $userRepository->findOneBy(["email" => $email]);

        if (!$user) {
            return $this->json(["code" => 404, "msg" => "User not found"], 404);
        }


        $password = $this->genearateNewPass();
        $user->setPassword($encoder->encodePassword($user, $password));


        $em->persist($user);
        $em->flush();

        //send email with new generated password
        $email = (new Email())
            ->from('contact@carthage-solutions.com')
            ->to('younes.maroine@gmail.com') // baddel el mail hadha
            ->subject('Password reset!')
            ->html('
                <p>Hello</p>
                <p>Following to your password reset request;<br>
                You will find attached a temporary password that you <b>must reset it as soon as you log in</b>.</p>
                <p>Email: admin-monastir@qualiteagilair.com<br>
                Password: ' . $password . '<br>
                Link website: <a href="https://admin-agil.qualiteagilair.today/login">https://admin-agil.qualiteagilair.today/</a></p>
                <p>Sincerely<br>
                qualiteagilair.today
                </p>
            ');

        $mailer->send($email);


        return $this->json(["code" => 200, "msg" => "Password resetted and email sent successfully, check your mailbox"], 200);
    }

    private function genearateNewPass()
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        $pass = substr(str_shuffle($permitted_chars), 0, 10);
        return $pass;
    }
}
