<?php

namespace App\Repository;

use App\Entity\History;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method History|null find($id, $lockMode = null, $lockVersion = null)
 * @method History|null findOneBy(array $criteria, array $orderBy = null)
 * @method History[]    findAll()
 * @method History[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, History::class);
    }

    // /**
    //  * @return History[] Returns an array of History objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    // findByDateAndStatus
    public function findByDateAndStatus($status, $date, $locationId): array
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT h, e, f, i, a, b, d, r, z, em
            FROM App\Entity\History h
            LEFT JOIN h.event e
            LEFT JOIN e.frequence f
            LEFT JOIN e.imprime i
            LEFT JOIN e.category a
            LEFT JOIN e.concerne b
            LEFT JOIN e.controle d
            LEFT JOIN e.responsible r
            LEFT JOIN e.location z
            LEFT JOIN e.emplacement em
            WHERE ((h.createdAt LIKE :date OR (h.status = false AND h.etat = true AND f.name != :freq) ) OR (h.validatedAt LIKE :date1 OR h.createdAt LIKE :date) ) AND z.id = :locationId '
            // WHERE h.createdAt LIKE :date OR  h.status = false"
        )->setParameters(["date" => "$date%", "date1" => "$date%", "locationId" => $locationId, "freq" => "Journalier"]);

        return $query->getArrayResult();
    }

    public function findByCategory()
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            "SELECT e, c
            FROM App\Entity\Evenement e
            LEFT JOIN e.category c
            WHERE c.name = :name
             "
        );
        return $query->getArrayResult();
    }
    public function findByLocation()
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            "SELECT e, l
            FROM App\Entity\Evenement e
            LEFT JOIN e.location l
            WHERE c.name = :name
             "
        );
        return $query->getArrayResult();
    }

    /*
    public function findOneBySomeField($value): ?History
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
