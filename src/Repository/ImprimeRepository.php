<?php

namespace App\Repository;

use App\Entity\Imprime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Imprime|null find($id, $lockMode = null, $lockVersion = null)
 * @method Imprime|null findOneBy(array $criteria, array $orderBy = null)
 * @method Imprime[]    findAll()
 * @method Imprime[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImprimeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Imprime::class);
    }
    public function findAllArray(): array
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT c
            FROM App\Entity\Imprime c'
        );

        return $query->getArrayResult();
    }

    public function findOneArray($id): array
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            "SELECT c
            FROM App\Entity\Imprime c WHERE c.id = $id"
        );

        return $query->getArrayResult();
    }

    // /**
    //  * @return Imprime[] Returns an array of Imprime objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Imprime
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
