<?php

namespace App\Repository;

use App\Entity\Evenement;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Evenement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Evenement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Evenement[]    findAll()
 * @method Evenement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvenementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Evenement::class);
    }

    public function findAllArray(): array
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT c, f, i, a, b, d, e,z,x
            FROM App\Entity\Evenement c
            LEFT JOIN c.frequence f
            LEFT JOIN c.imprime i
            LEFT JOIN c.category a
            LEFT JOIN c.concerne b
            LEFT JOIN c.controle d
            LEFT JOIN c.responsible e
            LEFT JOIN c.location z
            LEFT JOIN c.emplacement x'
        );

        return $query->getArrayResult();
    }

    public function findOneArray($id): array
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            "SELECT c,  f, i, a, b, d, e,z,x
            FROM App\Entity\Evenement c
            LEFT JOIN c.frequence f
            LEFT JOIN c.imprime i
            LEFT JOIN c.category a
            LEFT JOIN c.concerne b
            LEFT JOIN c.controle d
            LEFT JOIN c.responsible e
            LEFT JOIN c.location z
            LEFT JOIN c.emplacement x
             WHERE c.id = $id"
        );

        return $query->getArrayResult();
    }

    public function findByUser($locationId): array
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            "SELECT c,  f, i, a, b, d, e,z,x
            FROM App\Entity\Evenement c
            LEFT JOIN c.frequence f
            LEFT JOIN c.imprime i
            LEFT JOIN c.category a
            LEFT JOIN c.concerne b
            LEFT JOIN c.controle d
            LEFT JOIN c.responsible e
            LEFT JOIN c.location z
            LEFT JOIN c.emplacement x
             WHERE z.id = " . $locationId
        );
        return $query->getArrayResult();
    }

    public function findByName($name): array
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT e 
            FROM App\Entity\Evenement e
            WHERE e.name = :name'
        )->setParameter("name", $name);

        return $query->getResult();
    }


    public function findJournaliers(): array
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT e, f
            FROM App\Entity\Evenement e
            LEFT JOIN e.frequence f
            WHERE f.name = :name
            '
        )->setParameter("name", "journalier");

        return $query->getResult();
    }

    public function findHebdomadaire($day)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT e, f
            FROM App\Entity\Evenement e
            LEFT JOIN e.frequence f
            WHERE f.name = :name AND e.jour = :day
            '
        )->setParameters(["name" => "hebdomadaire", "day" => $day]);

        return $query->getResult();
    }


    public function findMensuel($day, $weekNumberInMonth)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT e, f
            FROM App\Entity\Evenement e
            LEFT JOIN e.frequence f
            WHERE f.name = :name AND e.jour = :day AND e.semaine = :semaine
            '
        )->setParameters(["name" => "mensuel", "day" => $day, "semaine" => $weekNumberInMonth]);

        return $query->getResult();
    }

    public function findTriSeAnOcc($date)
    {

        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT e, f
            FROM App\Entity\Evenement e
            LEFT JOIN e.frequence f
            WHERE (f.name = :name OR f.name = :name1 OR f.name = :name2 OR f.name = :name3 OR f.name = :name4 OR f.name = :name5 OR f.name = :name6 OR f.name = :name7)  AND e.date LIKE :date 
            '
        )->setParameters(["name" => "trimestriel", "name1" => "semestriel", "name2" => "annuel", "name3" => "occasionnel", "name4" => "02 ANS", "name5" => "03 ANS", "name6" => "05 ANS", "name7" => "10 ANS", "date" => $date]);

        return $query->getResult();
    }


    // public function findSemestriel($date)
    // {

    //     $em = $this->getEntityManager();
    //     $query = $em->createQuery(
    //         'SELECT e, f
    //         FROM App\Entity\Evenement e
    //         LEFT JOIN e.frequence f
    //         WHERE f.name = :name AND e.date LIKE :date 
    //         '
    //     )->setParameters(["name" => "semestriel", "date" => $date]);

    //     return $query->getResult();
    // }

    // public function findAnnuel($ann)
    // {

    //     $em = $this->getEntityManager();
    //     $query = $em->createQuery(
    //         'SELECT e, f
    //         FROM App\Entity\Evenement e
    //         LEFT JOIN e.frequence f
    //         WHERE f.name = :name AND e.date = :date 
    //         '
    //     )->setParameters(["name" => "annuel", "date" => $ann]);

    //     return $query->getResult();
    // }




    // /**
    //  * @return Evenement[] Returns an array of Evenement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Evenement
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
