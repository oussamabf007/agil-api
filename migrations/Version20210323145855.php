<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210323145855 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE evenement (id INT AUTO_INCREMENT NOT NULL, frequence_id INT NOT NULL, imprime_id INT NOT NULL, category_id INT NOT NULL, concerne_id INT NOT NULL, controle_id INT NOT NULL, responsible_id INT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, semaine VARCHAR(255) DEFAULT NULL, jour VARCHAR(255) DEFAULT NULL, date DATETIME DEFAULT NULL, INDEX IDX_B26681E8E487805 (frequence_id), INDEX IDX_B26681E6F5287D0 (imprime_id), INDEX IDX_B26681E12469DE2 (category_id), INDEX IDX_B26681E6406FEF1 (concerne_id), INDEX IDX_B26681E758926A6 (controle_id), INDEX IDX_B26681E602AD315 (responsible_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE evenement ADD CONSTRAINT FK_B26681E8E487805 FOREIGN KEY (frequence_id) REFERENCES frequence (id)');
        $this->addSql('ALTER TABLE evenement ADD CONSTRAINT FK_B26681E6F5287D0 FOREIGN KEY (imprime_id) REFERENCES imprime (id)');
        $this->addSql('ALTER TABLE evenement ADD CONSTRAINT FK_B26681E12469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE evenement ADD CONSTRAINT FK_B26681E6406FEF1 FOREIGN KEY (concerne_id) REFERENCES concerne (id)');
        $this->addSql('ALTER TABLE evenement ADD CONSTRAINT FK_B26681E758926A6 FOREIGN KEY (controle_id) REFERENCES controle (id)');
        $this->addSql('ALTER TABLE evenement ADD CONSTRAINT FK_B26681E602AD315 FOREIGN KEY (responsible_id) REFERENCES responsible (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE evenement');
    }
}
