<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210329142703 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE evenement DROP FOREIGN KEY FK_B26681E52F34864');
        $this->addSql('DROP INDEX IDX_B26681E52F34864 ON evenement');
        $this->addSql('ALTER TABLE evenement DROP historic_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE evenement ADD historic_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE evenement ADD CONSTRAINT FK_B26681E52F34864 FOREIGN KEY (historic_id) REFERENCES historic (id)');
        $this->addSql('CREATE INDEX IDX_B26681E52F34864 ON evenement (historic_id)');
    }
}
