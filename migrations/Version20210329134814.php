<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210329134814 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE historic (id INT AUTO_INCREMENT NOT NULL, valid TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE history (id INT AUTO_INCREMENT NOT NULL, event_id INT NOT NULL, status TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, validated_at DATETIME DEFAULT NULL, INDEX IDX_27BA704B71F7E88B (event_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE history_init (id INT AUTO_INCREMENT NOT NULL, date DATE NOT NULL, executed TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE history ADD CONSTRAINT FK_27BA704B71F7E88B FOREIGN KEY (event_id) REFERENCES evenement (id)');
        $this->addSql('ALTER TABLE evenement ADD historic_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE evenement ADD CONSTRAINT FK_B26681E64D218E FOREIGN KEY (location_id) REFERENCES location (id)');
        $this->addSql('ALTER TABLE evenement ADD CONSTRAINT FK_B26681E52F34864 FOREIGN KEY (historic_id) REFERENCES historic (id)');
        $this->addSql('CREATE INDEX IDX_B26681E64D218E ON evenement (location_id)');
        $this->addSql('CREATE INDEX IDX_B26681E52F34864 ON evenement (historic_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE evenement DROP FOREIGN KEY FK_B26681E52F34864');
        $this->addSql('DROP TABLE historic');
        $this->addSql('DROP TABLE history');
        $this->addSql('DROP TABLE history_init');
        $this->addSql('ALTER TABLE evenement DROP FOREIGN KEY FK_B26681E64D218E');
        $this->addSql('DROP INDEX IDX_B26681E64D218E ON evenement');
        $this->addSql('DROP INDEX IDX_B26681E52F34864 ON evenement');
        $this->addSql('ALTER TABLE evenement DROP historic_id');
    }
}
