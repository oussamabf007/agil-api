<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210326084009 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE location (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE evenement CHANGE frequence_id frequence_id INT DEFAULT NULL, CHANGE imprime_id imprime_id INT DEFAULT NULL, CHANGE category_id category_id INT DEFAULT NULL, CHANGE concerne_id concerne_id INT DEFAULT NULL, CHANGE controle_id controle_id INT DEFAULT NULL, CHANGE responsible_id responsible_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE location');
        $this->addSql('ALTER TABLE evenement CHANGE frequence_id frequence_id INT NOT NULL, CHANGE imprime_id imprime_id INT NOT NULL, CHANGE category_id category_id INT NOT NULL, CHANGE concerne_id concerne_id INT NOT NULL, CHANGE controle_id controle_id INT NOT NULL, CHANGE responsible_id responsible_id INT NOT NULL');
    }
}
